# mongodbapp

Mongodb digunakan untuk membuat aplikasi buku yang meliputi beberapa fungsi yaitu:
- Melihat semua daftar buku
    Untuk melihat daftar buku, gunakan REST API http://127.0.0.1:8000/books
- Melihat buku berdasarkan id buku
    Untuk melihat daftar buku berdasarkan id, gunakan REST API http://127.0.0.1:8000/bookbyid dan pada bagian Body, pilih raw, lalu pilih file JSON dan tuliskan { "_id":[id dari buku yang ingin ditampilkan]}
- Melihat buku berdasarkan nama buku
    Untuk melihat daftar buku berdasarkan nama buku, gunakan REST API http://127.0.0.1:8000/bookbyname dan pada bagian Body, pilih raw, lalu pilih file JSON dan tuliskan { "nama":[nama  dari buku yang ingin ditampilkan]}
